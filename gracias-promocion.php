<script> 
    var mensaje = sessionStorage.getItem("mensaje");
    if(!mensaje){
        window.location.href = "https://franciscoruizmalagon.com/promocion/";
    }
</script>
<html> 
    <head>
        <?php require('logica-formularios.php'); ?>
        <link rel=stylesheet href=style.css>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <title>Pagina de gracias</title>
    </head>

    <body>
        <!-- Banner -->
        <section> 
            <div class="container">
                <div class="background p-4">
                    <div class="row">
                        <div class="12">
                            <h1 class="border-bottom text-center red-light pb-2 mb-4">!Gracias por contactar con Nosotros!</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
    </body>
</html>