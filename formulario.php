<form class="row" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);  ?>" method="post">
    <div class="nombre col-6">
        <input type="text" name="nombre" placeholder="Nombre *" value="<?php echo $nombre ?>">
    </div>
    <div class="apellidos col-6">
        <input type="text" name="apellidos" placeholder="Apellidos *" value="<?php echo $apellidos ?>">
        <span class="error"> <?php echo $apellidosErr; ?></span>
    </div>
    <div class="telefono col-6">
        <input type="text" name="telefono" placeholder="Teléfono *" value="<?php echo $telefono ?>">
        <span class="error"><?php echo $telefonoErr; ?></span>
    </div>
    <div class="email col-6">
        <input type="text" name="email" placeholder="Email *" value="<?php echo $email ?>">
        <span class="error"><?php echo $emailErr; ?></span>
    </div>
    <div class="tipo-vehiculo col-12">
        <select type="text" name="tipo-vehiculo" onchange="this.form.submit()">
            <option <?php if($_POST['tipo-vehiculo'] == ""){ echo "selected"; } ?> value="elige" disabled>Elige el tipo de vehículo *</option>
            <option <?php if($_POST['tipo-vehiculo'] == "turismo"){ echo "selected"; } ?> value="turismo">Turismo</option>
            <option <?php if($_POST['tipo-vehiculo'] == "todo-terreno"){ echo "selected"; } ?> value="todo-terreno">Todo Terreno</option>
            <option <?php if($_POST['tipo-vehiculo'] == "comercial"){ echo "selected"; } ?> value="comercial">Comercial</option>
        </select>
        <span class="error"><?php echo $tipoVehiculoErr; ?></span>
    </div>
    <div class="vehiculo col-12">
        <select type="text" name="vehiculo" value="<?php echo $vehiculo ?>">
            <option value="elige" disabled selected >Elige tu vehiculo *</option>
            <option value="<?php echo $option1; ?>"><?php echo $option1; ?></option>
            <option value="<?php echo $option2; ?>"><?php echo $option2; ?></option>
        </select>
        <span class="error"><?php echo $vehiculoErr; ?></span>
    </div>
    <div class="horario col-12">
        <select id="horario" type="text" name="horario" value="<?php echo $horario ?>">
            <option value="elige" disabled  <?php if($horario == ""){ echo "selected"; } ?>>Elige tu horario</option>
            <option <?php if($horario == "mañana"){ echo "selected"; } if($horario == "tarde" || $horario == "noche"){ echo "disabled";} ?> name="option" value="mañana">Mañana</option>
            <option <?php if($horario == "tarde"){ echo "selected"; } if($horario == "mañana" || $horario == "noche"){ echo "disabled";} ?> name="option" value="tarde">Tarde</option>
            <option <?php if($horario == "noche"){ echo "selected"; } if($horario == "mañana" || $horario == "tarde"){ echo "disabled";} ?> name="option" value="noche">Noche</option>
        </select>
        <span class="error"><?php echo $horarioErr; ?></span>
    </div>
    <div class="col-12">
        <input class="boton" type="submit" value="Consigue tu descuento">
        <div class="spinner-border text-muted d-none"></div>
    </div>
</form>
</form>