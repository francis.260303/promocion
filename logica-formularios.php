<?php
//Definimos las variables
include('/conexion.php');

$nombreErr = ""; $apellidosErr=""; $telefonoErr =""; $emailErr=""; $tipoVehiculoErr =""; $vehiculoErr ="";
$nombre = ""; $apellidos=""; $telefono=""; $email=""; $tipoVehiculo = ""; $vehiculo =""; $select=""; $horario="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //Nombre
    if (empty($_POST["nombre"])) {
      $nombreErr = "Este campo es obligario";
    } else {
      $nombre = test_input($_POST["nombre"]);
    }
  
    //Apellidos
    if (empty($_POST["apellidos"])) {
      $apellidosErr = "Este campo es obligario";
    } else {
      $apellidos = test_input($_POST["apellidos"]);
    }
  
    //Teléfono
    if (empty($_POST["telefono"])) {
      $telefonoErr = "Este campo es obligario";
    } else {
      $telefono = test_input($_POST['telefono']);
      if (!preg_match('/^[967]\d{8}$/', $telefono)) {
        $telefonoErr = "El teléfono tiene 9 cifras y empieza por 9, 6 o 7";
      }
    }
  
    //Email
    if (empty($_POST["email"])) {
      $emailErr = "Este campo es obligario";
    } else {
      $email = test_input($_POST["email"]);
    }
  
    //Tipo de Vehiculo
    if (empty($_POST["tipo-vehiculo"])) {
      $tipoVehiculoErr = "Este campo es obligario";
    } else {
      $tipoVehiculo = test_input($_POST["tipo-vehiculo"]);
    }

  
    //Vehiculo
    if (empty($_POST["vehiculo"])) {
      $vehiculoErr = "Este campo es obligario";
    } else {
      $vehiculo = test_input($_POST["vehiculo"]);
    }
  
    if (empty($_POST["horario"])) {
    } else {
      $horario = test_input($_POST["horario"]);
    }
    
    if(!(empty($_POST["nombre"])) && !(empty($_POST["apellidos"])) && !(empty($_POST["telefono"])) && !(empty($_POST["email"])) && !(empty($_POST["tipo-vehiculo"])) && !(empty($_POST["vehiculo"]))){
        //Enviar Formulario
        $from = "motores@motores.es";
        $to = $email;
        $subject = "Has conseguido tu 50% en motores";
        $message = "Gracias por contactar. Has conseguido tu 50% en motores";
        mail($to,$subject,$message);

        //Insertamos datos
        $query = "INSERT INTO `contacto` VALUES ('$nombre', '$apellidos' , '$telefono', '$email' , '$tipoVehiculo' , '$vehiculo' , '$horario')";
        $result = mysqli_query($connection, $query);
      }
}


//Insertamos función para pintar Errores
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

//Insertar Vehiculos en el Select
if(isset($_POST['tipo-vehiculo'])){
    $select = $_POST['tipo-vehiculo'];
    switch ($select) {
        case 'turismo':
        $option1 = "corsa";
        $option2 = "astra";
        break;

        case 'todo-terreno':
            $option1 = "mokka";
            $option2 = "crossland";
        break;
        
        case 'comercial':
            $option1 = "corsa";
            $option2 = "astra";
            break;

        default:
            break;
    }
}

//Pasamos variables horario por URL
$horario = test_input($_GET["preferencia"]);

?>